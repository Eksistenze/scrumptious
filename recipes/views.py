from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


@login_required
def create_recipe(request):
    # if the HTTP request is POST
    if request.method == "POST":
        # create the variable "form" as the form information from the created website
        form = RecipeForm(request.POST)
        # check to see if it is valid.  "is_valid()" is a built in method
        if form.is_valid():
            # assign form data to recipe but don't save it
            recipe = form.save(False)
            # assign current user to recipe author
            recipe.author = request.user
            # save recipe(kind of like save form but form doesn't have an author, recipe does)
            recipe.save()
            # use redirect to go to a recipe_list webpage
            return redirect("recipe_list")
    else:
        # if it's not a POST, create a blank RecipeForm
        form = RecipeForm()
    # context is a naming convention to pass information to a template page
    context = {
        # creates a dict with the RecipeForm class in the variable "form"
        "form": form
    }
    # return the render with request, "website/path", context
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            redirect(show_recipe, id=id)
    else:
        form = RecipeForm(instance=recipe)

    context = {
        "recipe": recipe,
        "form": form,
    }

    return render(request, "recipes/edit.html", context)


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
